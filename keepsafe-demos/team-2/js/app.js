$(document).ready(function() {

	var $html = $("html");
	var $body = $("body");
	var $window = $(window);
	var window_w = $(window).width();

	var $profileCont = $(".profile-container");
	var $profile = $(".profile");

	var $bannerSection = $("section .banner");

	var $contentImg = $(".content-img");
	var $listItem = $(".list-item");

	var $section = $("section");
	var $animateSection = $(".animate-section");

	var $manifestoTitle = $(".manifesto-title");
	var $manifestoImg = $(".manifesto img");
	var $manifestoHeader = $(".manifesto-header");

	var $teamSection = $(".team-section");
	var $teamBgImg = $teamSection.find(".bg-img");
	var $team = $teamSection.find(".team");

	var $triggerTop = $('.trigger-top');
	var $triggerBottom = $('.trigger-bottom');
	var $triggerMiddle = $('.trigger-middle');

	var aTime = 500;
	var aType = "easeOutExpo";

	var aReady = true;
	var windowSize = "desktop";

	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)  {
   		$html.addClass("isSafari");
	}

	setup();

	function setup() {
		window_w = $(window).width();
		checkElems($triggerMiddle, $profileCont);
	}

	$(window).resize(function() {
		window_w = $(window).width();

		if (window_w > 990) {
			windowSize = "desktop";
		} else {
			windowSize = "mobile";
		}

	});


	$(window).scroll(function() {
		// checkElems($section, true);
		checkElems($triggerTop, $manifestoHeader);
		checkElems($triggerBottom, $profileCont);
		checkElems($triggerBottom, $contentImg);
		checkElems($triggerBottom, $listItem);
		checkElems($triggerMiddle, $manifestoTitle, true);

		// checkElems($triggerBottom, $team, true);

		// $team.each(function() {

		// 	if ($(this).hasClass("show")) {
		// 		var index = $team.index($(this));
		// 		console.log(index);
		// 		$teamBgImg.removeClass("show");
		// 		$teamBgImg.eq(index).addClass("show");
		// 	}
		// });

		$manifestoTitle.each(function () {
			if ($(this).hasClass("show")) {
				var index = $manifestoTitle.index($(this));
				// console.log(index);
				$manifestoImg.removeClass("show");
				$manifestoImg.eq(index).addClass("show");
			}
		});

		$("nav").addClass("show");

		// if ($teamSection.hasClass("show")) {
		// 	var $topAngle = $teamSection.find(".angle.top");
		// 	var angleTop = $topAngle.offset().top;
		// 	var top = $(window).scrollTop();
			// console.log(top);
			// console.log(angleTop);
			// if (top >= angleTop) {
			// 	$topAngle.addClass("fixed");
			// }
			// else {
			// 	$topAngle.removeClass("fixed");
			// }
		// }


		$animateSection.each(function () {
			var $this = $(this);
			var $thisAngle = $this.find(".angle");
			var $thisAngleTop = $this.find(".angle.top");
			var $thisAngleBottom = $this.find(".angle.bottom");

			if ($this.hasClass("show")) {
				var dist = ($triggerBottom.offset().top - $this.offset().top) / ($this.outerHeight());

				// var t1x = 0+"px";
				// var t1y = 100 + "%";
				// var t2x = 900*(1-dist) + 400 +"px";
				// var t2y = 50 + "%";
				// var t3x = "100%";
				// var t3y = "100%";
				// var t4x = 0+"px";
				// var t4y = "100%";

				// var b1x = 0 + "px";
				// var b1y = 0 + "%";
				// var b2x = 100 * dist + 170 + "px";
				// var b2y = 100 + "%";
				// var b3x = "100%";
				// var b3y = "100%";
				// var b4x = 0 + "px";
				// var b4y = "100%";

				var t1x = 0+"%";
				var t1y = 0 + "%";
				var t2x = 400*(1-dist) + 170 +"px";
				var t2y = 200 + "px";
				var t3x = "100%";
				var t3y = 100 - (50 * (1-dist)) + "%";
				var t3y = "100%";
				var t4x = "100%";
				var t4y = "100%";
				var t5x = 0 + "px";
				var t5y = "100%";

				// var clipPath = "polygon("+t1x+" "+t1y+", "+t2x+" "+t2y+", "+t3x+" "+t3y+", "+t4x+" "+t4y+")";
				// var clipPathBottom = "polygon(" + b1x + " " + b1y + ", " + b2x + " " + b2y + ", " + b3x + " " + b3y + ", " + b4x + " " + t4y + ")";

				var clipPath = "polygon(" + t1x + " " + t1y + ", " + t2x + " " + t2y + ", " + t3x + " " + t3y + ", " + t4x + " " + t4y + "," + t5x + " " + t5y + ")";

				$thisAngleTop.css({"clip-path":clipPath});
				$thisAngleBottom.css({"clip-path":clipPath});
			}
		});

	});

	function checkElems($_trigger, $_elem, _hide = false) {
		$_elem.each(function () {
			var $this = $(this);

			if ($this.offset().top <= $_trigger.offset().top) {

				if (!$this.hasClass('show')) {
					$this.addClass('show');
					if (_hide) {
						$_elem.removeClass("show");
						// $this.addClass('show').siblings().removeClass('show');
						$this.addClass("show");
					}
				}
			}
		});
	}




});