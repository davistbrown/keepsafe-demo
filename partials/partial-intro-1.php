<section class="content-section bg-purple">
		<div class="container">

			<?php
			$titles = [
					"Keepsafe protects your personal space. ",
					"Our mission is making privacy and security simple.",
					"In an era that’s over valued sharing, privacy is the new freedom. ",
					"You need to feel safe and free to be yourself.",
					"Set your own boundaries, organize your life and control your privacy.",
					"Share only the things you wish with only those you trust.",
					"We give you protected space on your devices to keep important things safe.",
					"Keepsafe puts your privacy first to make space for the real you.",
				];
			?>

			<div class="row justify-content-start align-items-center pb-5 manifesto-header">
				<h1 class="display-3">The Keepsafe Manifesto</h1>
			</div>

			<div class="w-100 py-5"></div>

			<?php

				$tc = 0;
				foreach ($titles as $title) {
					$tc++;
					$sideClass = "justify-content-start";
					if ($tc % 2 == 0) {
						$sideClass = "justify-content-end";
					}
				?>


				<div class="row manifesto-title <?php echo $sideClass ?> align-items-center  py-5">
					<div class="col-4 position-absolute">
						<img class="img-fluid show" src="images/manifesto/ic-manifesto-0<?php echo $tc ?>.png" alt="">
					</div>
					<div class="col-6">
						<h1 style="font-weight: bold" class=" mb-0">
							<?php echo $title ?>
						</h1>
					</div>
				</div>

			<?php } ?>


		</div>
		<div class="angle bottom bg-white"></div>
	</section>