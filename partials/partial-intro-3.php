<section class="content-section bg-purple">
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-4">
				<img class="img-fluid position" src="images/backgrounds/ic-manifesto-01@2x.png" alt="">
			</div>
			<div class="col-6">
				<h1 class="display-3 mb-3 gradient-text">The Keepsafe Manifesto</h1>
				<h4 style="color:#C5BCE5; line-height: 150%">
						<strong>Keepsafe protects your personal space. In an era that’s over valued sharing, privacy is the new freedom.  Our mission is making privacy and security simple. </strong>
						<br>
						<br>
						You need to feel safe and free to be yourself. Set your own boundaries, organize your life and control your privacy. Share only the things you wish with only those you trust. We give you protected space on your devices to keep important things safe. Keepsafe puts your privacy first to make space for the real you.
				</h4>
			</div>

		</div>
	</div>
	<div class="angle bottom bg-white"></div>
</section>