<!DOCTYPE html>
<html lang="en-us">
<head>
	<title>Web Demo</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<meta http-equiv="content-language" content="en-US"/>
	<meta http-equiv="window-target" content="_top"/>
	<meta name="robots" content="index, follow" />
	<meta name="target" content="all"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="subject" content="UCLA Alpert"/>


	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/app.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900" rel="stylesheet">

	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/easing.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>
	<script type="text/javascript" src="../js/app.js"></script>

</head>