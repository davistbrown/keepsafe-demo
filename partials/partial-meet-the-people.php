<section class="content-section bg-purple">
		<!-- <div class="angle top bg-white"></div> -->
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-4">
					<img src="images/other/pattern@2x.png" alt="" class="img-fluid">
				</div>
				<div class="col-6">
					<h1>Meet the people who live for making your privacy and security simple </h1>
				</div>
			</div>
		</div>
	</section>