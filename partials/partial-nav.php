<div class="trigger trigger-top js-trigger"></div>
<div class="trigger trigger-middle js-trigger"></div>
<div class="trigger trigger-bottom js-trigger"></div>

<nav>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-auto mr-auto">
				<img class="img-fluid" src="images/other/logo@2x.png" alt="">
			</div>
			<div class="nav-item">Home</div>
			<div class="nav-item active">Products</div>
			<div class="nav-item">About</div>
			<div class="nav-item">Blog</div>
			<div class="nav-item">Press</div>
		</div>
	</div>
</nav>