<section class="content-section">
	<div class="angle top"></div>
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-6">
					<h1>
						How we do it
					</h1>
					<h5 class="lead">
						Keepsafe uses cipher AES-256 encryption, considered among the most secure in the world and “bank-level” or “military-grade” across all of its privacy and security apps.
					</h5>

					<div class="row flex-column mt-1">
						<div class="mt-2 font-weight-bold list-item">
							<div class="row no-gutters align-items-center">
								<div class="col-auto">
									<img src="images/icons/pin-code@2x.png" class="mr-1" alt="">
								</div>
								<div class="col">
									<strong>Military-grade encryption</strong>
									</br>
									Our back-ups are also encrypted, which are managed by your device and by Keepsafe’s back-up system
								</div>
							</div>
						</div>
						<div class="mt-2 font-weight-bold list-item">
							<div class="row no-gutters">
								<div class="col-auto">
									<img src="images/icons/touch@2x.png" class="mr-1" alt="">
								</div>
								<div class="col">
									<strong>Encrypted backup</strong>
									</br>
									Our back-ups are also encrypted with multiple layers of encryption keys
								</div>
							</div>
						</div>
						<div class="mt-2 font-weight-bold list-item">
							<div class="row no-gutters">
								<div class="col-auto">
									<img src="images/icons/shield@2x.png" class="mr-1" alt="">
								</div>
								<div class="col">
									<strong>No access for Keepsafe employers</strong>
									</br>
									We have systems in place that prevent Keepsafe emploer’s access to your content
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-4">
					<img class="content-img img-fluid rounded" src="images/other/phone@2x.png" alt="">
				</div>
			</div>
		</div>
	</section>