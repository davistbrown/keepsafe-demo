<section class="banner-section bg-purple team-section team-1 large dark-gradient">
		<!-- <div class="fixed-bar"></div> -->
		<!-- <div class="angle top"></div> -->
		<img class="bg-img img-fluid show" src="images/backgrounds/sf-overlay.jpg" alt="">
		<img class="bg-img img-fluid" src="images/backgrounds/krakow-overlay.jpg" alt="">
		<img class="bg-img img-fluid" src="images/backgrounds/berlin-overlay.jpg" alt="">
		<img class="bg-img img-fluid" src="images/backgrounds/global-support-overlay.jpg" alt="">

			<div class="container">

				<h1 class="mb-5 display-3 team">San Francisco</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 13; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>

				<h1 class="my-5 display-3 team">Krakow</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 7; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>

				<h1 class="my-5 display-3 team">Berlin</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 7; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>

				<h1 class="my-5 display-3 team">Global Support</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 13; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		<!-- <div class="angle bottom bg-white"></div> -->
	</section>