<section class="content-section bg-purple">
		<div class="container">

			<?php
			$titles = [
					"Keepsafe protects your personal space. ",
					"Our mission is making privacy and security simple.",
					"In an era that’s over valued sharing, privacy is the new freedom. ",
					"You need to feel safe and free to be yourself.",
					"Set your own boundaries, organize your life and control your privacy.",
					"Share only the things you wish with only those you trust.",
					"We give you protected space on your devices to keep important things safe.",
					"Keepsafe puts your privacy first to make space for the real you.",
				];
			?>

			<div class="row justify-content-center align-items-center py-5">
				<h1 class="display-3">The Keepsafe Manifesto</h1>
				<div class="col-3 position-fixed manifesto">

					<img class="img-fluid show" src="images/manifesto/ic-manifesto-01.png" alt="">
					<img class="img-fluid" src="images/manifesto/ic-manifesto-02.png" alt="">
					<img class="img-fluid" src="images/manifesto/ic-manifesto-03.png" alt="">
					<img class="img-fluid" src="images/manifesto/ic-manifesto-04.png" alt="">
					<img class="img-fluid" src="images/manifesto/ic-manifesto-05.png" alt="">
					<img class="img-fluid" src="images/manifesto/ic-manifesto-06.png" alt="">
					<img class="img-fluid" src="images/manifesto/ic-manifesto-07.png" alt="">
					<img class="img-fluid" src="images/manifesto/ic-manifesto-08.png" alt="">

				</div>
			</div>

			<?php

				$tc = 0;
				foreach ($titles as $title) {
					$tc++;
				?>

					<div class="row justify-content-center align-items-center text-center py-8">
						<div class="col-6">
							<h1 style="font-weight: bold" class="manifesto-title mb-0">
								<?php echo $title ?>
							</h1>
						</div>
					</div>

			<?php } ?>


		</div>
		<div class="angle bottom bg-white"></div>
	</section>