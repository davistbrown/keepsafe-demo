<style>
		.sf-team {
			background-image: url(images/backgrounds/sf-overlay.jpg);
			background-size: cover;
			background-attachment: fixed;
		}
	</style>
	<section class="banner-section team-section dark-gradient sf-team">
		<img class="bg-img img-fluid show" src="images/backgrounds/sf-overlay.jpg" alt="">
		<div class="container">
		<h1 class="mb-5 display-3 team">San Francisco</h1>
			<div class="row justify-content-between">
				<?php
				$i = 1;
				for($i; $i < 13; $i++) {
					$rand = rand(1,11);
					?>
					<div class="col-lg-3 col-6 profile-container">
						<!-- <div class="slice"></div> -->
						<div class="profile">
							<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
						</div>

						<h4>First Last</h4>
						<p>Job Title</p>
					</div>
					<?php
						if ($i % 3 == 0) { ?>
						<div class="w-100"></div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</section>

	<style>
	.krakow-team {
		background-image: url(images/backgrounds/krakow-overlay.jpg);
		background-size: cover;
		background-attachment: fixed;
	}
	</style>
	<section class="banner-section team-section dark-gradient krakow-team">
		<!-- <img class="bg-img img-fluid show" src="images/backgrounds/sf-overlay.jpg" alt=""> -->
		<div class="container">
		<h1 class="mb-5 display-3 team">Krakow</h1>
			<div class="row justify-content-between">
				<?php
				$i = 1;
				for($i; $i < 7; $i++) {
					$rand = rand(1,11);
					?>
					<div class="col-lg-3 col-6 profile-container">
						<!-- <div class="slice"></div> -->
						<div class="profile">
							<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
						</div>

						<h4>First Last</h4>
						<p>Job Title</p>
					</div>
					<?php
						if ($i % 3 == 0) { ?>
						<div class="w-100"></div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</section>

	<style>
		.berlin-team {
			background-image: url(images/backgrounds/berlin-overlay.jpg);
			background-size: cover;
			background-attachment: fixed;
		}
	</style>
	<section class="banner-section team-section dark-gradient berlin-team">
		<!-- <img class="bg-img img-fluid show" src="images/backgrounds/berlin-overlay.jpg" alt=""> -->
		<div class="container">
		<h1 class="mb-5 display-3 team">Berlin</h1>
			<div class="row justify-content-between">
				<?php
				$i = 1;
				for($i; $i < 4; $i++) {
					$rand = rand(1,11);
					?>
					<div class="col-lg-3 col-6 profile-container">
						<!-- <div class="slice"></div> -->
						<div class="profile">
							<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
						</div>

						<h4>First Last</h4>
						<p>Job Title</p>
					</div>
					<?php
						if ($i % 3 == 0) { ?>
						<div class="w-100"></div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</section>

	<style>
	.global-support-team {
		background-image: url(images/backgrounds/global-support-overlay.jpg);
		background-size: cover;
		background-attachment: fixed;
	}
	</style>
	<section class="banner-section team-section bg-purple global-support-team">
		<!-- <img class="bg-img img-fluid show" src="images/backgrounds/berlin-overlay.jpg" alt=""> -->
		<div class="container">
		<h1 class="mb-5 display-3 team">Global Support</h1>
			<div class="row justify-content-between">
				<?php
				$i = 1;
				for($i; $i < 13; $i++) {
					$rand = rand(1,11);
					?>
					<div class="col-lg-3 col-6 profile-container">
						<!-- <div class="slice"></div> -->
						<div class="profile">
							<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
						</div>

						<h4>First Last</h4>
						<p>Job Title</p>
					</div>
					<?php
						if ($i % 3 == 0) { ?>
						<div class="w-100"></div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</section>