<section class="banner-section dark-gradient" style="z-index: 0">
	<img class="bg-img img-fluid show" src="images/backgrounds/bridge@2x.png" alt="">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1>Working at Keepsafe</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<h3>Interested in joining the Keepsafe team? We're always looking for talented people to join us.</h3>
			</div>

		</div>
		<div class="row">
			<div class="col">
				<div class="btn mt-2 p-1" style="background: #F3F3F3">
				Learn More
				</div>
			</div>
		</div>
	</div>
	<div class="angle bottom bg-white"></div>
</section>

