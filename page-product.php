<?php include("partials/partial-head.php") ?>

<body onunload="">

	<?php include("partials/partial-nav.php") ?>

	<section class="banner-section dark-gradient">
		<img class="bg-img show" src="images/backgrounds/photo-vault@2x.png" alt="">

		<div class="container">
			<img src="images/icons/app-icon@2x.png" class="mb-2" alt="">
			<div class="w-100"></div>
			<h1 class="mb-0 font-weight-bold gradient-text">Photo Vault</h1>
			<h1 class="mt-1 mb-1 display-3 font-weight-bold">Control your privacy</h1>
			<h3 style="font-weight: 300">Lock down private photos, vidoes, files and documents</h3>
			<div class="row mt-3">
				<div class="col-auto">
					<div class="btn">
						<div class="row no-gutters p-1 justify-content-center align-items center">
							<img src="images/icons/apple@2x.png" class="mr-1" alt="">
							View on App Store
						</div>
					</div>
				</div>
				<div class="col-auto">
					<div class="btn">
						<div class="row no-gutters p-1 justify-content-center align-items center">
							<img src="images/icons/google-play@2x.png" class="mr-1" alt="">
							Get it on Play Store
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="angle bottom bg-white"></div>
	</section>

	<section class="content-section">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-6">
					<h1>
						Trusted by millions to hide and secures personal items
					</h1>
					<h5 class="lead">
						Back-up photos with cloud storage and save space
						on your phone. Invite friends to protected, shared
						albums to view and comment on photos.
					</h5>

					<div class="row flex-column mt-1">
						<div class="mt-2 font-weight-bold list-item">
							<img src="images/icons/pin-code@2x.png" class="mr-1" alt="">
							PIN code password protection
						</div>
						<div class="mt-2 font-weight-bold list-item">
							<img src="images/icons/touch@2x.png" class="mr-1" alt="">
							Touch ID
						</div>
						<div class="mt-2 font-weight-bold list-item">
							<img src="images/icons/shield@2x.png" class="mr-1" alt="">
							Military-grade encryption
						</div>

					</div>
				</div>
				<div class="col-4">
					<img class="content-img img-fluid img-shadow rounded" src="images/other/app@2x.png" alt="">
				</div>
			</div>
		</div>
	</section>

	<section class="content-section bg-purple animate-section">
		<div class="angle top bg-white"></div>
		<div class="container">
			<!-- <div class="row justify-content-center">
				<h1 class="display-2">Our Plans</h1>
				<div class="w-100 mb-5"></div>
			</div> -->
			<div class="row justify-content-around">
				<div class="col-4">
					<img src="images/other/basic@2x.png" alt="" class="img-fluid content-img rounded basic" style="border: 2px solid #ffffff40">
				</div>
				<div class="col-auto align-self-center text-center">
					<h2 style="font-weight: bold; opacity:.5;">or</h2>
				</div>
				<div class="col-4">
					<img src="images/other/premium@2x.png" alt="" class="img-fluid content-img rounded premium" style="border: 2px solid #fff">
				</div>
			</div>
		</div>
		<div class="angle bottom bg-white"></div>
	</section>


	<section class="content-section">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-6">
					<h1>Keepsafe Calculator</h1>
					<div class="row no-gutters">
						<h5 class="lead col-9">
							Photo Vault with a decoy app icon and the same privacy and security features. <strong>Hide secret pictures so they won’t be exposed in your photo gallery.</strong>
						</h5>
						<div class="btn mt-2" style="background: #F3F3F3">
							<div class="row no-gutters p-1 justify-content-center align-items center">
								<img src="images/icons/google-play@2x.png" class="mr-1" alt="">
								Get it on Play Store
							</div>
						</div>
					</div>
				</div>
				<div class="col-4">
					<img class="img-fluid content-img img-shadow rounded" src="images/other/calc@2x.png" alt="">
				</div>
			</div>
		</div>
	</section>


</body>
</html>
