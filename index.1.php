<?php include("partials/partial-head.php") ?>

<body onunload="">

	<?php include("partials/partial-nav.php") ?>

	<!-- <section class="content-section bg-purple">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-4 position-absolute">
					<img class="img-fluid" src="images/backgrounds/ic-manifesto-01.png" alt="">
				</div>
				<h1 class="display-3">The Keepsafe Manifesto</h1>
			</div>

			<div class="row justify-content-center text-center mt-8">
				<div class="col-7">
					<h4 style="line-height: 150%">
							Keepsafe protects your personal space. In an era that’s over valued sharing, privacy is the new freedom. <strong> Our mission is making privacy and security simple. </strong> You need to feel safe and free to be yourself. Set your own boundaries, organize your life and control your privacy. Share only the things you wish with only those you trust. We give you protected space on your devices to keep important things safe. Keepsafe puts your privacy first to make space for the real you.
					</h4>
				</div>
			</div>
		</div>
		<div class="angle bottom bg-white"></div>
	</section> -->


	<section class="content-section bg-purple">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-4">
					<img class="img-fluid position" src="images/backgrounds/ic-manifesto-01@2x.png" alt="">
				</div>
				<div class="col-6">
					<h1 class="display-3 mb-3 gradient-text">The Keepsafe Manifesto</h1>
					<h4 style="color:#C5BCE5; line-height: 150%">
							<strong>Keepsafe protects your personal space. In an era that’s over valued sharing, privacy is the new freedom.  Our mission is making privacy and security simple. </strong>
							<br>
							<br>
							You need to feel safe and free to be yourself. Set your own boundaries, organize your life and control your privacy. Share only the things you wish with only those you trust. We give you protected space on your devices to keep important things safe. Keepsafe puts your privacy first to make space for the real you.
					</h4>
				</div>

			</div>
		</div>
		<div class="angle bottom bg-white"></div>
	</section>


	<section class="content-section">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-6">
					<h1>
						How we do it
					</h1>
					<h5 class="lead">
						Keepsafe uses cipher AES-256 encryption, considered among the most secure in the world and “bank-level” or “military-grade” across all of its privacy and security apps.
					</h5>

					<div class="row flex-column mt-1">
						<div class="mt-2 font-weight-bold list-item">
							<div class="row no-gutters align-items-center">
								<div class="col-auto">
									<img src="images/icons/pin-code@2x.png" class="mr-1" alt="">
								</div>
								<div class="col">
									<strong>Military-grade encryption</strong>
									</br>
									Our back-ups are also encrypted, which are managed by your device and by Keepsafe’s back-up system
								</div>
							</div>
						</div>
						<div class="mt-2 font-weight-bold list-item">
							<div class="row no-gutters">
								<div class="col-auto">
									<img src="images/icons/touch@2x.png" class="mr-1" alt="">
								</div>
								<div class="col">
									<strong>Encrypted backup</strong>
									</br>
									Our back-ups are also encrypted with multiple layers of encryption keys
								</div>
							</div>
						</div>
						<div class="mt-2 font-weight-bold list-item">
							<div class="row no-gutters">
								<div class="col-auto">
									<img src="images/icons/shield@2x.png" class="mr-1" alt="">
								</div>
								<div class="col">
									<strong>No access for Keepsafe employers</strong>
									</br>
									We have systems in place that prevent Keepsafe emploer’s access to your content
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-4">
					<img class="content-img img-fluid rounded" src="images/other/phone@2x.png" alt="">
				</div>
			</div>
		</div>
	</section>

	<section class="content-section bg-purple">
		<div class="angle top bg-white"></div>
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<div class="col-4">
					<img src="images/other/pattern@2x.png" alt="" class="img-fluid">
				</div>
				<div class="col-6">
					<h1>Meet the people who live for making your privacy and security simple </h1>
				</div>
			</div>
		</div>

	</section>

	<section class="banner-section team-section dark-gradient">
		<img class="bg-img img-fluid show" src="images/backgrounds/sf-overlay.jpg" alt="">
		<div class="container">
		<h1 class="mb-5 display-3 team">San Francisco</h1>
			<div class="row justify-content-between">
				<?php
				$i = 1;
				for($i; $i < 13; $i++) {
					$rand = rand(1,11);
					?>
					<div class="col-lg-3 col-6 profile-container">
						<!-- <div class="slice"></div> -->
						<div class="profile">
							<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
						</div>

						<h4>First Last</h4>
						<p>Job Title</p>
					</div>
					<?php
						if ($i % 3 == 0) { ?>
						<div class="w-100"></div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</section>

	<section class="banner-section bg-purple team-section large dark-gradient">
		<!-- <div class="fixed-bar"></div> -->
		<div class="angle top"></div>
		<img class="bg-img img-fluid show" src="images/backgrounds/sf-overlay.jpg" alt="">
		<img class="bg-img img-fluid hide" src="images/backgrounds/krakow-overlay.jpg" alt="">
		<img class="bg-img img-fluid hide" src="images/backgrounds/berlin-overlay.jpg" alt="">

			<div class="container">

				<h1 class="mb-5 display-3 team">San Francisco</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 13; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>

				<h1 class="my-5 display-3 team">Krakow</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 4; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>

				<h1 class="my-5 display-3 team">Berlin</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 7; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>

				<h1 class="my-5 display-3 team">Global Support</h1>
				<div class="row justify-content-between">
					<?php
					$i = 1;
					for($i; $i < 13; $i++) {
						$rand = rand(1,11);
						?>
						<div class="col-lg-3 col-6 profile-container">
							<!-- <div class="slice"></div> -->
							<div class="profile">
								<img src="images/profiles/circles/<?php echo $rand ?>@2x.png" alt="profile picture">
							</div>

							<h4>First Last</h4>
							<p>Job Title</p>
						</div>
						<?php
							if ($i % 3 == 0) { ?>
							<div class="w-100"></div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		<!-- <div class="angle bottom bg-white"></div> -->
	</section>



	<section class="banner-section dark-gradient" style="z-index: 0">
		<img class="bg-img img-fluid show" src="images/backgrounds/bridge@2x.png" alt="">
		<div class="container">
			<div class="row">
				<div class="col">
					<h1>Working at Keepsafe</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<h3>Interested in joining the Keepsafe team? We're always looking for talented people to join us.</h3>
				</div>

			</div>
			<div class="row">
				<div class="col">
					<div class="btn mt-2 p-1" style="background: #F3F3F3">
					Learn More
					</div>
				</div>
			</div>
		</div>
		<div class="angle bottom bg-white"></div>
	</section>


	<section class="content-section">

	</section>

</body>
</html>
